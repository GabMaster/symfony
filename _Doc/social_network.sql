-- phpMyAdmin SQL Dump
-- version 4.2.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-03-2020 a las 22:54:03
-- Versión del servidor: 5.7.19
-- Versión de PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `social_network`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `following`
--

CREATE TABLE IF NOT EXISTS `following` (
`id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `followed` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
`id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `publication_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `readed` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `extra` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `private_messages`
--

CREATE TABLE IF NOT EXISTS `private_messages` (
`id` bigint(20) NOT NULL,
  `message` longtext,
  `emitter` bigint(20) DEFAULT NULL,
  `receiver` bigint(20) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `readed` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publications`
--

CREATE TABLE IF NOT EXISTS `publications` (
`id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `publication` mediumtext,
  `document` varchar(255) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` bigint(20) NOT NULL,
  `rol` varchar(100) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `surmane` varchar(45) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `nick` varchar(45) DEFAULT NULL,
  `bografia` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `imagen` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `rol`, `email`, `name`, `surmane`, `password`, `nick`, `bografia`, `active`, `imagen`) VALUES
(1, 'ROLE ADMIN', 'admin@local.com', 'Luis', 'Avila', '$2y$04$zcRsNwKz4NvHwwVJoiFC6e0EQTARtBfqOkubeUAAo/xoYLPOGmqlu', 'luis', 'Desarrollador de sistemas', 1, NULL),
(2, 'ROLE ADMIN', 'carlos@gmail.com', 'Carlos', 'Rincon', '$2y$04$zcRsNwKz4NvHwwVJoiFC6e0EQTARtBfqOkubeUAAo/xoYLPOGmqlu', 'carlosr', NULL, 1, NULL),
(3, 'ROLE USER', 'pedro@lopez.com', 'Pedro', 'Lopez', '$2y$04$3zaF0wr.ANZRM5hzNlzOlOb/dtkajoHPckp.ycNWUEgM4A2tJuYF6', 'plopez', NULL, 1, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `following`
--
ALTER TABLE `following`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_following_users1_idx` (`user_id`), ADD KEY `fk_following_users2_idx` (`followed`);

--
-- Indices de la tabla `likes`
--
ALTER TABLE `likes`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_likes_users1_idx` (`user_id`), ADD KEY `fk_likes_publications1_idx` (`publication_id`);

--
-- Indices de la tabla `notifications`
--
ALTER TABLE `notifications`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_notifications_users1_idx` (`user_id`);

--
-- Indices de la tabla `private_messages`
--
ALTER TABLE `private_messages`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_private_messages_users1_idx` (`emitter`), ADD KEY `fk_private_messages_users2_idx` (`receiver`);

--
-- Indices de la tabla `publications`
--
ALTER TABLE `publications`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_publications_users_idx` (`user_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `following`
--
ALTER TABLE `following`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `likes`
--
ALTER TABLE `likes`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `private_messages`
--
ALTER TABLE `private_messages`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `publications`
--
ALTER TABLE `publications`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `following`
--
ALTER TABLE `following`
ADD CONSTRAINT `fk_following_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_following_users2` FOREIGN KEY (`followed`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `likes`
--
ALTER TABLE `likes`
ADD CONSTRAINT `fk_likes_publications1` FOREIGN KEY (`publication_id`) REFERENCES `publications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_likes_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `notifications`
--
ALTER TABLE `notifications`
ADD CONSTRAINT `fk_notifications_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `private_messages`
--
ALTER TABLE `private_messages`
ADD CONSTRAINT `fk_private_messages_users1` FOREIGN KEY (`emitter`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_private_messages_users2` FOREIGN KEY (`receiver`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `publications`
--
ALTER TABLE `publications`
ADD CONSTRAINT `fk_publications_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
