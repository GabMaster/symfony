<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use BackendBundle\Entity\User;
use AppBundle\Form\RegisterType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends Controller
{
    private $session;

    public function __construct()
    {
       $this->session = new Session();
    }

    public function loginAction(Request $request)
    {
      $authenticationUtils = $this->get('security.authentication_utils');
      $error = $authenticationUtils->getLastAuthenticationError();
      $lastUsername = $authenticationUtils->getLastUsername();
        //die;
        return $this->render('AppBundle:User:login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
          ]);
    }

    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);

        $form->handleRequest($request);
        if($form->isSubmitted()) {
            if($form->isValid()) {

                $em = $this->getDoctrine()->getManager();
                //$user_repo = $em->getRepository('BackendBundle:User');
                $query = $em->createQuery('SELECT u FROM BackendBundle:User u WHERE u.email = :email OR u.nick = :nick')
                    ->setParameter('email', $form->get('email')->getData())
                    ->setParameter('nick', $form->get('nick')->getData());

                $user_isset = $query->getResult();

                if(!count($user_isset)) {
                    $factory = $this->get('security.encoder_factory');
                    $encoder = $factory->getEncoder($user);
                    $password = $encoder->encodePassword($form->get('password')->getData(), $user->getSalt());

                    $user->setPassword($password);
                    $user->setRol('ROLE USER');
                    $user->setImagen(null);

                    $em->persist($user); //persiste en doctrine
                    $flush = $em->flush(); //pasarlo a la base de datos

                    if($flush == null) {
                        $status = 'Te has registrado correctamente';
                        $this->session->getFlashBag()->add('status', $status);
                        return $this->redirect('login');
                    } else {
                        $status = 'No te has registrado correctamente'; 
                    }


                } else {
                    $status = 'El usuario ya existe'; 
                }

                $this->session->getFlashBag()->add('status', $status);

            } else {
               $status = 'No te has registrado correctamente.'; 
            }
        }

      return $this->render('AppBundle:User:register.html.twig', array('form' => $form->createView()));
    }

    public function nickTestAction(Request $request)
    {
       $nick = $request->get('nick');
       $em = $this->getDoctrine()->getManager();
       $user_repo = $em->getRepository('BackendBundle:User');
       $user_isset = $user_repo->findOneBy(['nick' => $nick]);
       $response = false;
       $msg = '';
       if(count($user_isset) && is_object($user_isset)) {
           $response = true;
           $msg = 'El nick no esta disponible';
       }
       //return new Response(json_encode(['response' => $response, 'msg' => $msg]));
       return new JsonResponse((['response' => $response, 'msg' => $msg]));
    }


}
