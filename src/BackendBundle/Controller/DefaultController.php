<?php

namespace BackendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
    	//permite acceder a las entidades
    	$em = $this->getDoctrine()->getManager();
    	$user_repo =  $em->getRepository('BackendBundle:User');
    	//$user = $user_repo->findAll();
    	$user = $user_repo->find(1);
    	echo "<pre>";
    	print_r($user);
    	exit;
       return $this->render('BackendBundle:Default:index.html.twig');
    }
}
