<?php

namespace BackendBundle\Entity;

/**
 * User
 */
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $rol;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $surmane;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $nick;

    /**
     * @var string
     */
    private $bografia;

    /**
     * @var boolean
     */
    private $active = '1';

    /**
     * @var string
     */
    private $imagen;


    public function getUsername()
    {
        return $this->email;
    }

    public function getSalt()
    {
        return null;
    }

    public function getRoles()
    {
        //return $this->getRole();
        return ['ROLE USER', 'ROLE ADMIN'];
    }

    public function eraseCredentials()
    {
       
    }

    public function __toString()
    {
        return $this->name;
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rol
     *
     * @param integer $rol
     *
     * @return User
     */
    public function setRol($rol)
    {
        $this->rol = $rol;

        return $this;
    }

    /**
     * Get rol
     *
     * @return integer
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surmane
     *
     * @param string $surmane
     *
     * @return User
     */
    public function setSurmane($surmane)
    {
        $this->surmane = $surmane;

        return $this;
    }

    /**
     * Get surmane
     *
     * @return string
     */
    public function getSurmane()
    {
        return $this->surmane;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set nick
     *
     * @param string $nick
     *
     * @return User
     */
    public function setNick($nick)
    {
        $this->nick = $nick;

        return $this;
    }

    /**
     * Get nick
     *
     * @return string
     */
    public function getNick()
    {
        return $this->nick;
    }

    /**
     * Set bografia
     *
     * @param string $bografia
     *
     * @return User
     */
    public function setBografia($bografia)
    {
        $this->bografia = $bografia;

        return $this;
    }

    /**
     * Get bografia
     *
     * @return string
     */
    public function getBografia()
    {
        return $this->bografia;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set imagen
     *
     * @param string $imagen
     *
     * @return User
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }
}

