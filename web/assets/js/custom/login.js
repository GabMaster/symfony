$(document).ready(function () {
    //validar si el nick existe
    $('.nick-input').on('blur', function () {
        var nick = $(this);
        $.ajax({
            url: main_url+'app_dev.php/nick-test',
            data:{nick: nick.val()},
            type: 'POST',
            success: function (response) {
                //response = JSON.parse(response)
                if(response.response) {
                    nick.css('border', '1px solid red');
                } else {
                    nick.css('border', '1px solid green');
                }
            }
        })
    })
})
